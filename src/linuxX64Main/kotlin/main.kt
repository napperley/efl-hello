package org.example.eflHello

import efl.*
import kotlinx.cinterop.*
import kotlin.system.exitProcess

private val globalArena = Arena()

fun main(args: Array<String>) {
    val argsPtr = args.toCStringArray(globalArena)
    val totalArgs = args.size
    // Initialize the application.
    initEcore()
    addMainLoopCallback()
    // Pass through the program arguments.
    ecore_init_ex(totalArgs, argsPtr)

    ecore_con_init()
    ecore_con_url_init()
    // Initialize the UI system.
    elm_init(totalArgs, null)
    // Start the main event loop.
    efl_loop_begin(efl_main_loop_get())
}

private fun initEcore() {
    if (ecore_init() < 1) {
        println("Cannot initialize application")
        exitProcess(-1)
    }
}

private fun addMainLoopCallback() {
    eflEventCallbackAdd(
        obj = efl_main_loop_get(),
        desc = EFL_LOOP_EVENT_ARGUMENTS,
        callback = staticCFunction(::eflMain),
        data = null
    )
}

private fun addWindowCloseCallback(win: CPointer<Eo>?) {
    eflEventCallbackAdd(
        obj = win,
        desc = EFL_UI_WIN_EVENT_DELETE_REQUEST,
        callback = staticCFunction(::quitApplication)
    )
}

private fun addQuitButtonClickCallback(btn: CPointer<Eo>?) {
    eflEventCallbackAdd(
        obj = btn,
        desc = EFL_INPUT_EVENT_CLICKED,
        callback = staticCFunction(::quitApplication)
    )
}

@Suppress("UNUSED_PARAMETER")
private fun eflMain(data: COpaquePointer?, event: CPointer<Efl_Event>?) {
    setupGui()
}

@Suppress("UNUSED_PARAMETER")
private fun quitApplication(data: COpaquePointer?, event: CPointer<Efl_Event>?) {
    efl_exit(0)
}

private fun eflEventCallbackAdd(
    obj: CPointer<Eo>?,
    desc: CPointer<Efl_Event_Description>?,
    callback: Efl_Event_Cb,
    data: COpaquePointer? = null
) = efl_event_callback_priority_add(
    obj = obj,
    desc = desc,
    priority = EFL_CALLBACK_PRIORITY_DEFAULT.toShort(),
    cb = callback,
    data = data
) == 1.toUByte()

private fun setupGui() = memScoped {
    val minSize = alloc<Eina_Size2D>().apply {
        w = 360
        h = 240
    }
    val mainLoop = efl_main_loop_get() ?: throw IllegalStateException("The main loop cannot be null")
    val win = eflAdd(classType = EFL_UI_WIN_CLASS, parent = mainLoop) { obj ->
        efl_text_set(obj, "Hello World")
        addWindowCloseCallback(obj)
    }
    val box = eflAdd(classType = EFL_UI_BOX_CLASS, parent = win) { obj ->
        efl_content_set(win, obj)
        efl_gfx_hint_size_min_set(obj, minSize.readValue())
    }
    eflAdd(classType = EFL_UI_BUTTON_CLASS, parent = box) { obj ->
        efl_text_set(obj, "Quit")
        efl_gfx_hint_weight_set(obj = obj, x = 1.0, y = 0.1)
        efl_pack(box, obj)
        addQuitButtonClickCallback(obj)
    }
}

private fun eflAdd(
    classType: CPointer<Efl_Class>?,
    parent: CPointer<Eo>,
    isRef: Boolean = false,
    init: (CPointer<Eo>?) -> Unit
): CPointer<Eo> {
    val obj = _efl_add_internal_start(
        file = "",
        line = 0,
        klass_id = classType,
        parent = parent,
        ref = isRef.uByteValue,
        is_fallback = EINA_TRUE
    )
    init(obj)
    return _efl_add_end(obj = _efl_added_get(), is_ref = isRef.uByteValue, is_fallback = EINA_TRUE)
        ?: throw IllegalStateException("The added object cannot be null")
}

private val Boolean.uByteValue: UByte
    get() = if (this) EINA_TRUE else EINA_FALSE
