package org.example.eflHello

import efl.EFL_INPUT_EVENT_CLICKED
import efl.EFL_UI_WIN_EVENT_DELETE_REQUEST
import io.gitlab.embedSoft.eflKt.core.event.EflEvent
import io.gitlab.embedSoft.eflKt.core.event.EflEventHandlerRegistry
import io.gitlab.embedSoft.eflKt.core.exitApplication

internal val winHandlerReg by lazy {
    val result = EflEventHandlerRegistry()
    result.register(EFL_UI_WIN_EVENT_DELETE_REQUEST, ::quitApplication)
    result
}
internal val btnHandlerReg by lazy {
    val result = EflEventHandlerRegistry()
    result.register(EFL_INPUT_EVENT_CLICKED, ::quitApplication)
    result
}

@Suppress("UNUSED_PARAMETER")
private fun quitApplication(event: EflEvent) {
    exitApplication()
}
