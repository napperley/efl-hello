package org.example.eflHello

import efl.EFL_INPUT_EVENT_CLICKED
import efl.EFL_UI_WIN_EVENT_DELETE_REQUEST
import efl.efl_content_set
import efl.efl_text_interactive_selection_allowed_set
import io.gitlab.embedSoft.eflKt.core.dataType.graphics.EinaSize2D
import io.gitlab.embedSoft.eflKt.core.dataType.uByteValue
import io.gitlab.embedSoft.eflKt.core.getMainLoop
import io.gitlab.embedSoft.eflKt.ui.layout.box
import io.gitlab.embedSoft.eflKt.ui.text.textMarkup
import io.gitlab.embedSoft.eflKt.ui.widget.button
import io.gitlab.embedSoft.eflKt.ui.widget.textBox
import io.gitlab.embedSoft.eflKt.ui.widget.window

private val minSize by lazy {
    EinaSize2D.create().apply {
        width = 360
        height = 240
    }
}
private val mainLoop by lazy { getMainLoop() }
private val win by lazy {
    window(parent = mainLoop) {
        text = "Hello World"
        addEventHandler(EFL_UI_WIN_EVENT_DELETE_REQUEST, winHandlerReg)
    }
}
private val box by lazy {
    box(parent = win) {
        efl_content_set(win.eoPtr, eoPtr)
        hintSizeMin = minSize
    }
}

internal fun setupMainWindow() {
    textBox(parent = box) {
        textMarkup = "Hello World. This is an <b>Efl.Ui</b> application!"
        efl_text_interactive_selection_allowed_set(eoPtr, false.uByteValue)
        hintWeight = 1.0 to 0.9
        hintAlign = 0.5 to 0.5
        box.pack(this)
    }
    button(parent = box) {
        text = "Quit"
        hintWeight = 1.0 to 0.1
        box.pack(this)
        addEventHandler(EFL_INPUT_EVENT_CLICKED, btnHandlerReg)
    }
}
